import React, { Component } from 'react'
import Draggable from 'react-draggable'
import './styles/main.css'
const Box = ({ depth, unitSize }) => (
  <Draggable cancel={`#box-${depth - 1}`} bounds="parent">
    <div
      className="box bg-indigo-200 rounded-sm border border-indigo-900"
      id={`box-${depth}`}
      style={{ height: unitSize * depth, width: unitSize * depth }}
    >
      <span className="text-gray-900 text-lg">{depth}</span>
      {(depth > 1) ? <Box depth={depth - 1} unitSize={unitSize} /> : null}
    </div>
  </Draggable>
)

class App extends Component {
  state = {
    depth: 2
  }

  addWindow = () => {
    this.setState({ depth: this.state.depth + 1 })
  }

  render() {
    return (
      <div className="flex flex-col bg-gray-100 items-stretch h-screen">
        <div className="flex flex-col bg-gray-100 items-center text-5xl">
          <h1 className="text-gray-700 text-center mb-5 font-hairline">Draggy</h1>
          <button
            className="text-gray-200 text-center text-xl bg-blue-400 border border-blue-600 px-2 py-1 mb-10 rounded-sm uppercase"
            onClick={this.addWindow}
          >Add Window</button>
        </div>
        <div className="flex-grow bg-gray-200">
          <Box depth={this.state.depth} unitSize={100} />
        </div>
      </div>
    );
  }
}

export default App;
