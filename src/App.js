import React from 'react'
import Draggable from 'react-draggable'
import './DraggableRecursion.css'

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      depth: 2
    }
  }

  addWindow = () => {
    this.setState({ depth: this.state.depth + 1 })
  }

  render() {
    return (
      <div>
        <button onClick={this.addWindow}>Add Window</button>
        <div style={{ width: '100vw', height: '100vh', position: 'absolute' }}>
          <Box depth={this.state.depth} unitSize={100} />
        </div>
      </div>
    )
  }

}

const Box = ({ depth, unitSize }) => (
  <Draggable cancel={`#box-${depth - 1}`} bounds="parent">
    <div className="box" id={`box-${depth}`} style={{ height: unitSize * depth, width: unitSize * depth }}>
      <span>{depth}</span>
      {(depth > 1) ? <Box depth={depth - 1} unitSize={unitSize} /> : null}
    </div>
  </Draggable>
)

export default App;